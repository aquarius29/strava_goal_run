<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Models\MyUsers as MyUsers;
use Illuminate\Http\Request;

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/', ['as' => 'strava_welcome', 'uses' => 'StravaCtrl@index'] );

// Route::get('/strava_runs', 'StravaCtrl@displayData');
Route::get('/strava_runs', ['as' => 'strava_runs', 'uses' => 'StravaCtrl@displayData']);

Route::post('/strava_runs', 'StravaCtrl@setGoal');
