@extends('layouts.basic_layout');


<!-- @section('', 'You have run') -->
@section('title')
	<?php 

	?>
	Hi {{ $name }}	
	<br>
	Set your goal for {{ $year}} (km)
	<form action='/strava_runs' method='POST' >
		<input type="number" name="goal">
		<input type="number" name="user_id" hidden="true" value="{{ $user_id }}">
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />		
		<input type="submit" name="submit" value="Done!">
	</form>
@endsection

