@extends('layouts.basic_layout');


<!-- @section('', 'You have run') -->
@section('title')
	Hi {{ $name }}	
	<br>
	Your distance is <b id='distance'>{{ $distance }}</b> km
@endsection

@section('content')
	<?php
	if( $estimate_km > $distance){
		$color = "red";
	}else{
		$color = "green";
	}
	?>
	by today ( {{ $today_date }} ) you should have covered <b style='color:{{$color}}'>{{ $estimate_km }}</b> km!
	<div id='explanation2'>You need to run <b>{{$daily_distance}}</b> km every day to reach the goal</div>
	<div id='explanation'></div>
@endsection

@section('js')
<script type="text/javascript">
$("#distance").attr("onclick","change_value(this)");
// $("#daily_distance").attr("onclick","change_value_dd(this)");

var distance_full =  "{{$distance}}"
var distance_round = "{{$distance_rounded}}"

var daily_distance =  "{{$daily_distance}}"
var daily_distance_rounded = "{{$daily_distance_rounded}}"

function change_value(el){
	if($(el)[0].innerText== distance_full){
		$(el)[0].innerHTML = distance_round + "*"
		$('#explanation')[0].innerText = "* - total distance for challenge2017 (rounded to full km).";
		$('#explanation2')[0].innerHTML = "You need to run <b>" + daily_distance_rounded + "</b> km every day to reach the goal";
	}else{
		$(el)[0].innerHTML = distance_full
		$('#explanation')[0].innerText = ""
		$('#explanation2')[0].innerHTML = "You need to run <b>" + daily_distance + "</b> km every day to reach the goal";
		// $(el).replaceWith("<b id='distance'>" +  distance_full + "</b>")	
	}
}



	
</script>
@endsection