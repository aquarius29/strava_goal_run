<?php

namespace App\Http\Controllers;

use Storage;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Cache;
use App\Models\MyUsers as MyUsers;
use App\Models\UserData as UserData;
use App\Models\ApiTokens as ApiTokens;
use DateTime;
use Iamstuartwilson\StravaApi as StravaApi;

class StravaCtrl extends Controller
{
	private $api;
	private $athlete;

	function __construct(){
		date_default_timezone_set('Europe/Stockholm');	
		$api = new StravaApi(
		    env('STRAVA_ID'),
		    env('STRAVA_SECRET')
		);
		$this->api = $api;
		$this->athlete = null; 
	}

	function index(){
		$api = $this->api;
		$redirect_url = env('APP_URL') . "/strava_runs";
		$url = $api->authenticationUrl($redirect = $redirect_url,  $approvalPrompt = 'auto', $scope = "view_private", $state = null);
		$img_url = Storage::url('loginwithstrava.png');
		// echo ' about to call strava strava_welcome';
		return view('strava_welcome', compact('url', 'img_url'));
	}

	function setAthlete($athlete){
		$this->athlete = $athlete;
	}

	function getToken(Request $request){
		$api = $this->api;
		if(!isset($_GET['code'])){
			// echo "  hiiiiii ";
			redirect()->route('strava_welcome');
		}else{
			// echo " hooooooo " ;
			$code = $_GET['code'];
			$json = $api->tokenExchange($code) ;
			$access_token =  $json -> access_token ;
			$api->setAccessToken($access_token);
			$this->saveTokenForUser($access_token);
			$request->session()->put('strava_access_token', $access_token);
		}
	}

	function displayData(Request $request){
		// echo " running display data "; 

		$api = $this->api;
		$access_token = $request->session()->get('strava_access_token');
		$api->setAccessToken($access_token);
		if(!isset($_GET['code']) and !$request->session()->has('strava_access_token')){
		// if(!$request->session()->has('strava_access_token')){
			// echo " running getToken() ";

			return redirect()->route('strava_welcome');
		}
		// echo ' getting token : ';
		$this->getToken($request);
		$activities = $api->get("athlete/activities", ['after'=>1483228800, 'per_page'=>200]);
		if (count($activities)==200){
			$activities = $this->getActivitiesBeyond200($activities);
		}
		// echo " activity 199 : " ;
		// var_dump($activities[199]);
		// echo " ===================";
		// var_dump($activities);
		$athlete = $api->get("athlete");
		$this->setAthlete($athlete);
		$name = $this->getName($athlete);

		$distance  			= $this->getDistance($activities); 
		$distance_rounded   = $this->getDistanceRounded($activities); 
		$estimate_km = $this->getEstimatedDistance();
		$today_date = date(' M j');
		$user_id = $this->userExists($athlete); 
		$daily_distance 		= $this->getDailyRunToChatchup($user_id, $distance);
		$daily_distance_rounded = $this->getDailyRunToChatchup($user_id, $distance_rounded);
		$request->session()->put('user_id', $user_id);
		$user_goal = $this->getGoal($user_id);

		if (!$user_goal) {
			// echo "    WTF " ;
			return view('set_goal', compact('name', 'user_id') );
		}

		// echo " this should return the strava_runs view";
		return view('strava_runs', compact('distance', 'distance_rounded', 'today_date', 'estimate_km', 'name', 'daily_distance', 'daily_distance_rounded') );
	}

	function getName($athlete){
		return $athlete->firstname;
	}


	function getDistanceRounded($activities){
		$distance = 0;
		for($i=0; $i < count($activities); $i++){
			$act = $activities[$i];
			if($act->type=="Run"){
				$distance+= intval( ($act->distance)/1000 );
			}
		}
		return $distance;
	}

	function getDistance($activities){
		$distance = 0;
		for($i=0; $i < count($activities); $i++){
			$act = $activities[$i];
			if($act->type=="Run"){
				$distance+= $act->distance/1000;
			}
		}
		return number_format($distance, 1, ".","");
	}

	function getEstimatedDistance(){
		$athlete = $this->athlete;
		if(!$athlete){
			// echo " fishy .... can't get this->athlet in getesitmateddistance";
			// return Route::get('/', 'StravaCtrl@index');;
		}else{
			$user_id = $this->userExists($athlete);
			$goal = $this->getGoal($user_id);
			$days = 365;
			$day_today = date('z')+1;
			return intval($goal/$days* $day_today);

		}
	}

	function userExists($athlete){
		$email = $athlete->email;
		$user = MyUsers::where('email', $email)->first();

		if($user){
			return $user->id;
		}else{
			return false;
		}
	}

	function saveUser($athlete){
		$user = new MyUsers;
		$user->email =  $athlete->email;
		$user->name = $athlete->firstname;
		$user->password = 'password';
		$user->save();
		return $user->id;
	}

	function saveTokenForUser($strava_token){
		$api = $this->api;
		$athlete = $api->get("athlete");
		$user_id = $this->userExists($athlete);
		if(!$user_id){
			$user_id = $this->saveUser($athlete);
		}

		$token = new ApiTokens;

		if(!$this->getStravaTokenForUser($user_id)){
			$token->access_token = $strava_token;
			$token->token_provider = "strava";
			$token->user_id = $user_id;
			$token->save();
		}else{
			$token = ApiTokens::where('user_id', $user_id)->where('token_provider', "strava")->update(['access_token'=> $strava_token]);
			// $token->access_token = $strava_token;
			// $token->save();
		}
	}

	function getStravaTokenForUser($user_id){
		$api_token = ApiTokens::where('user_id', $user_id)->where('token_provider', "strava")->first();
		if(!$api_token){
			return false;
		}else{
			return $api_token->access_token;
		}
	}


	function setGoal(Request $request){
			$user_id = $request->session()->get('user_id');
			$goal = $_POST['goal'];

			$data = new UserData;
			$data->user_id = $user_id;
			$data->goal = $goal;
			$data->save();
			return redirect()->route('strava_runs');
			// $this->displayData($request);
	}

	function getGoal($user_id){
		$user = UserData::where("user_id", $user_id)->first(); 
		if($user){
			return $user->goal;
		}else{
			return $user;
		}
	}

	function getDailyRunToChatchup($user_id, $distance){
		$goal = $this->getGoal($user_id);
		$days_left = 365 - date('z')+1;
		return number_format(($goal - $distance)/$days_left, 2, ".", ""); 
	}

	function getActivitiesBeyond200($activities){
		$api = $this->api;
		$act = $activities;
		do{
			$date = new DateTime($act[199]->start_date);
			$after = $date->getTimeStamp();
			$act = $api->get("athlete/activities", ['after'=>$after, 'per_page'=>200]);
			$activities = array_merge($activities, $act);
		} while (count($act) == 200);
		return $activities;
	}
}

